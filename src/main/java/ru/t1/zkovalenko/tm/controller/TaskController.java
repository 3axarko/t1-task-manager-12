package ru.t1.zkovalenko.tm.controller;

import ru.t1.zkovalenko.tm.api.controller.ITaskController;
import ru.t1.zkovalenko.tm.api.service.ITaskService;
import ru.t1.zkovalenko.tm.constant.ConsoleColorConst;
import ru.t1.zkovalenko.tm.enumerated.Status;
import ru.t1.zkovalenko.tm.model.Task;
import ru.t1.zkovalenko.tm.util.ColorizeConsoleTextUtil;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService tasktService) {
        this.taskService = tasktService;
    }

    @Override
    public void createTask(){
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.err.println("[ERROR]\nName is required field");
        else ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void clearTasks(){
        System.out.println("[CLEAR TASK]");
        taskService.clear();
        ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void showTasks(){
        System.out.println("[TASK LIST]");
        final List<Task> tasks = taskService.findAll();
        int index = 1;
        for(final Task task: tasks){
            System.out.println(index + ". " + task);
            index++;
        }
        ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void showTask(final Task task){
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }

    @Override
    public void showTaskById(){
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null){
            System.err.println("FAIL");
            return;
        }
        showTask(task);
        ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void showTaskByIndex(){
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null){
            System.err.println("FAIL");
            return;
        }
        showTask(task);
        ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void removeTaskById(){
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeById(id);
        if (task == null) System.err.println("[FAIL]");
        else ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void removeTaskByIndex(){
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.removeByIndex(index);
        if (task == null) System.err.println("[FAIL]");
        else ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void updateTaskById(){
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateById(id, name, description);
        if (task == null) System.err.println("[FAIL]");
        else ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void updateTaskByIndex(){
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateByIndex(index, name, description);
        if (task == null) System.err.println("[FAIL]");
        else ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void startTaskById(){
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.changeTaskStatusById(id, Status.IN_PROGRESS);
        if (task == null) System.err.println("[FAIL]");
        else ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void startTaskByIndex(){
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.changeTaskStatusByIndex(index, Status.IN_PROGRESS);
        if (task == null) System.err.println("[FAIL]");
        else ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void completeTaskById(){
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.changeTaskStatusById(id, Status.COMPLETED);
        if (task == null) System.err.println("[FAIL]");
        else ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void completeTaskByIndex(){
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.changeTaskStatusByIndex(index, Status.COMPLETED);
        if (task == null) System.err.println("[FAIL]");
        else ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void changeStatusById(){
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.println("AVAILABLE STATUSES:");
        System.out.println(Arrays.toString(Status.values()));
        System.out.println("ENTER STATUS: ");
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Task task = taskService.changeTaskStatusById(id, status);
        if (task == null) System.err.println("[FAIL]");
        else ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void changeStatusByIndex(){
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("AVAILABLE STATUSES:");
        System.out.println(Arrays.toString(Status.values()));
        System.out.println("ENTER STATUS: ");
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Task task = taskService.changeTaskStatusByIndex(index, status);
        if (task == null) System.err.println("[FAIL]");
        else ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

}
