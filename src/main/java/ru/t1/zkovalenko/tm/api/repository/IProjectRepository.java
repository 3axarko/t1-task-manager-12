package ru.t1.zkovalenko.tm.api.repository;

import ru.t1.zkovalenko.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    Project add(Project project);

    void clear();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project removeById(String id);

    Project removeByIndex(Integer index);
}
