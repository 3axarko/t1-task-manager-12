package ru.t1.zkovalenko.tm.api.controller;

public interface ICommandController {

    void showError();

    void showArguments();

    void showCommands();

    void showVersion();

    void showAbout();

    void showHelp();

    void showInfo();

    void showWelcome();

}
